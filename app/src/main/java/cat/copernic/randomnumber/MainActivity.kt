package cat.copernic.randomnumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cat.copernic.randomnumber.ui.theme.RandomNumberTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RandomNumberTheme {
                RandomNumberApp()
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RandomNumberApp(){
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = "Random Numbers",
                        fontWeight = FontWeight.Bold
                    )
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
                    actionIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer
                )
            )
        }
    ){ innerPadding ->

        Surface (
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)
                .background(MaterialTheme.colorScheme.tertiaryContainer)
        ) {
            Column(

                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ){

                ContentOfRandomApp()

            }

        }
    }

}

@Composable
fun ContentOfRandomApp(){
    val context = LocalContext.current
    var textFrom by remember { mutableStateOf("") }
    var textTo by remember { mutableStateOf("") }
    val focusRequester = remember { FocusRequester() }

    val onAccept:()->Unit = {
        when{
            textFrom.isEmpty() -> Toast.makeText(context, "Field can't be empty", Toast.LENGTH_SHORT).show()
            textTo.isEmpty() -> Toast.makeText(context, "Field can't be empty", Toast.LENGTH_SHORT).show()
            textFrom.toInt() > textTo.toInt() -> Toast.makeText(context, "From is greater than to", Toast.LENGTH_SHORT).show()
            else -> runIntent(textFrom,textTo,context)
        }
    }

    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(16.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically

    ) {
        OutlinedTextField(
            value = textFrom,
            onValueChange = {if(it.toIntOrNull() != null || it.isEmpty()) textFrom = it
            else Toast.makeText(context, "Only Integer", Toast.LENGTH_SHORT).show()},

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions(onNext = {
                focusRequester.requestFocus()
            }),
            label = {Text(stringResource(R.string.TextFrom))},
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        )
        Spacer(modifier = Modifier.width(10.dp))
        OutlinedTextField(
            value = textTo,
            onValueChange = {if(it.toIntOrNull() != null || it.isEmpty()) textTo = it
            else Toast.makeText(context, "Only Integer", Toast.LENGTH_SHORT).show()},

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Send
            ),
            keyboardActions = KeyboardActions(onSend = {  onAccept() }),
            label = {Text(stringResource(R.string.TextTo))},
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .focusRequester(focusRequester)
        )
    }
    Button(onClick = onAccept) {
        Text(
            text = stringResource(R.string.btn_Send),
            fontWeight = FontWeight.Bold
        )
    }
}

fun runIntent(textFrom:String,textTo:String,context:Context){
    val numbers = arrayOf(textFrom,textTo)
    val intent= Intent(context,
        SecondActivity::class.java)
    intent.putExtra("Numbers",numbers)
    context.startActivity(intent)
}

@Preview
@Composable
fun AppPreview(){
    RandomNumberTheme {
        RandomNumberApp()
    }
}

