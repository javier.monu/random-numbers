package cat.copernic.randomnumber

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import cat.copernic.randomnumber.ui.theme.RandomNumberTheme


var numbers: List<Int>? = null
var rNumber: Int = 0

class SecondActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RandomNumberTheme {
                numbers = intent.getStringArrayExtra("Numbers")?.map { it.toInt() }
                val (fN,fS) = numbers?: arrayListOf(0,0)
                rNumber = (fN..fS).random()
                RandomApp2()
            }
        }
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RandomApp2(){
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = "Random Numbers",
                        fontWeight = FontWeight.Bold
                    )
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
                    actionIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer
                )
            )
        },
        floatingActionButton = {
            FloatingActionButton(onClick = { /*TODO*/ }) {
                Icon(Icons.Default.ArrowBack, contentDescription = "back")
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ){ innerPadding ->

        Surface (
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)
                .background(MaterialTheme.colorScheme.tertiaryContainer)
        ) {
            Column(

                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ){

            ContentOfApp2()
            }

        }
    }
}

@Composable
fun ContentOfApp2(){
    val context = LocalContext.current
    var numberToCompare by remember { mutableStateOf("") }
    var isCorrect by remember { mutableStateOf(false) }
    var tries by remember { mutableIntStateOf(0) }

    val send = {
        val (fN,fS) = numbers?: arrayListOf(0,0)

        when{
            numberToCompare.isEmpty()-> Toast.makeText(context, "Field can't be empty",
                Toast.LENGTH_SHORT).show()
            numberToCompare.toInt() !in fN..fS -> Toast.makeText(context,
                "$numberToCompare isn't between $fN and $fS", Toast.LENGTH_SHORT).show()
            else-> {
                if(numberToCompare.toInt() == rNumber){
                    isCorrect = true
                }else{
                    if (tries < 3)tries++
                }
            }
        }
    }

    Text(
        stringResource(R.string.SecondTitle),
        fontWeight = FontWeight.Bold
    )

    OutlinedTextField(
        value = numberToCompare,
        onValueChange = {if((it.toIntOrNull() != null || it.isEmpty()) && !isCorrect ) numberToCompare = it
        else Toast.makeText(context, "Only Integer", Toast.LENGTH_SHORT).show()},
        label = { Text(stringResource(R.string.SecondSFild))},
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number,
                                            imeAction = ImeAction.Next),
        keyboardActions = KeyboardActions(onNext = { send() })
    )
    Button(onClick = { send() }) {
            Text(stringResource(R.string.btn_Try))
    }
    if (tries != 0 || isCorrect) TextOfCorrect(isCorrect,tries)
    TextOfTry( isCorrect, tries)
}

@Composable
fun TextOfCorrect(isCorrect:Boolean,tries: Int){
    val text:String = when{
        tries == 3-> stringResource(R.string.lose)
        isCorrect-> stringResource(R.string.win)
        else -> stringResource(R.string.isNotCorrect)
    }
    Text(
        text = text,
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp
        )
}

@Composable
fun TextOfTry(isCorrect:Boolean,tries:Int){

    if (tries != 0){
        Text(
            text = "Try $tries",
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp
        )
    }
}

@Preview
@Composable
fun Random2Preview(){
    RandomNumberTheme {
        RandomApp2()
    }
}